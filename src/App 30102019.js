import React from 'react';
import axios from 'axios';
import './App.css';
import _ from 'lodash';


class App extends React.Component {
  constructor(props){
    super(props);
    this.state = {
      lists: [],
      name:'',
      isSubmitted: false,
      savedName:''
    }
    this.handleChange=this.handleChange.bind(this);
    this.handleEdit=this.handleEdit.bind(this);
    this.handleDelete=this.handleDelete.bind(this);

  }

  // handle edit button
  handleEdit = event =>{

  }

  // handle delete buttton
  handleDelete = id => {
    console.log(id)
  }

  // change name state value handler
  handleChange = event => {
    this.setState({
      name: event.target.value
    });
  }

  // submit function handler
  handleSubmit = event =>{
    console.log("you submit")
    event.preventDefault();
    const listData = {};

    listData['name'] = this.state.name;

    axios.post('https://todolist-16ba5.firebaseio.com/list.json?auth=QimTYGjP02LUarjINcaSftII3X4bb1OZ5myEsWDU', listData)
      .then(res => {
        return res;
      });
    this.setState({
      isSubmitted: true,
      savedName:this.state.name
    })


  }
  componentDidMount() {
    
    axios.get(`https://todolist-16ba5.firebaseio.com/list.json?auth=QimTYGjP02LUarjINcaSftII3X4bb1OZ5myEsWDU&print=pretty`)
      .then(res => {
        if (res.data != null) {
          
        // const list = Object.entries(res.data).map(([k, v]) => ({[k]: v}));
        const list = Object.keys(res.data).map(id => ({ id, name: res.data[id].name }))
        // const list = Object.fromEntries(Object.entries(res.data).map((k, v) => [k, {v}]));
        this.setState({lists: list});
        console.log(this.state.lists)

        }
      })
  }
  componentDidUpdate(prevState) {
    // Typical usage (don't forget to compare props):
    if (this.state.name !== prevState.name) {
      axios.get(`https://todolist-16ba5.firebaseio.com/list.json?auth=QimTYGjP02LUarjINcaSftII3X4bb1OZ5myEsWDU&print=pretty`)
      .then(res => {
        const list = _.toArray(res.data);
        this.setState({lists: list});

      })
    }
  }

  render() {
    return (
      <div>

      <form onSubmit={this.handleSubmit}>
        <input value={this.state.name} onChange={this.handleChange}/><br></br>
        <button type='submit'>Submit!</button>
      </form>
      {this.state.isSubmitted && <Results name={this.state.savedName}/>}
        <ul>
          { this.state.lists.map((data, i,j) => <ListRow deleteUser={this.handleDelete} data = {data}/>)}
        </ul>
      </div>
    )
  }
}

class Results extends React.Component{
  render(){
    return(
      <div>
        <p>{this.props.name} has been added</p>
      </div>
    )
  }
}
class ListRow extends React.Component {

  render() {

     return (
       
       <li>{this.props.data.name}<button  onClick={this.props.deleteUser(this.props.data.id)}>delete</button></li> 

     );
  }
}

export default App;
