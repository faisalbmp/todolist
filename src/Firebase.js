import * as firebase from 'firebase';
import firestore from 'firebase/firestore'

const settings = {timestampsInSnapshots: true};

const firebaseConfig = {
    apiKey: "AIzaSyC_MS2leT-oxkJ7yOERKkmS6IOycICbpnw",
    authDomain: "todolist-16ba5.firebaseapp.com",
    databaseURL: "https://todolist-16ba5.firebaseio.com",
    projectId: "todolist-16ba5",
    storageBucket: "todolist-16ba5.appspot.com",
    messagingSenderId: "200338861562",
    appId: "1:200338861562:web:db08ae3d4c30cfd9a50843",
    measurementId: "G-HDD9Z5R3FL"
  };
firebase.initializeApp(firebaseConfig);

firebase.firestore().settings(settings);

export default firebase;